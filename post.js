'use strict';
// set GOOGLE_APPLICATION_CREDENTIALS=C:/Users/User/Desktop/express/service.json  <setting environment variable in cmd>
// console.log(process.env.GOOGLE_APPLICATION_CREDENTIALS);
function getRandomNumber(min, max) {
   const decimal= Math.random() * (max - min) + min;
   return Math.trunc(decimal)
}

function getRandomString(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function main() {
    const {BigQuery} = require('@google-cloud/bigquery');
    async function getTables() {
        const bigqueryClient = new BigQuery();
        const sqlQuery = `INSERT INTO \`qai-insurance-health-dev.dash_app.tab1\`
                        VALUES (
             @bot_id,
             @participant_id,
             @timestamp,
             @session_id,
             @message_id,
             @response_id,
             @query_text,
             @reply_text,
             @create_time,
             @end_time,
             @lang_code,
             @isFallBack,
             @intent_id,
             @intent_display_name
                )`
        const options = {
        query: sqlQuery,
        location: 'US',
        params: {
        bot_id: getRandomNumber(1,10).toString(),
        participant_id: getRandomNumber(1,10).toString(),
        timestamp: getRandomNumber(1546300800,1577836799).toString(),
        session_id: getRandomNumber(1,20).toString(),
        message_id: getRandomNumber(1,100).toString(),
        response_id: '1',
        query_text: getRandomString(getRandomNumber(1,20)),
        reply_text: getRandomString(getRandomNumber(1,20)),
        create_time: getRandomNumber(1546300800,1577836799).toString(),
        end_time: getRandomNumber(1546300800,1577836799).toString(),
        // end_time: '',
        lang_code: 'en',
        isFallBack: Math.random() >= 0.5,
        intent_id: getRandomNumber(1,10).toString(),
        intent_display_name: getRandomString(getRandomNumber(1,10)) },
        };
        const [rows] = await bigqueryClient.query(options);
        console.log('DATA SENT SUCCESSFULLY');
    }
    getTables();
  }

main()
