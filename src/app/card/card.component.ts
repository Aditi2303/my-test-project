import { Component, OnInit, Input } from '@angular/core';

export interface CardDetails {
  title: string,
  subtitle: string,
  content: string,
  link: string,
  sublink?: string
}

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input('cardDetails') cardDetails: CardDetails;

  constructor() { }

  ngOnInit() {
  }

}
