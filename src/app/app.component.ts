import { Component } from '@angular/core';
import { CardDetails } from './card/card.component';
import { Chart } from 'chart.js';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'nav';
  cards: Array<CardDetails> = [
    {
      title: "Total Sessions",
      subtitle: "Test Subtitle",
      content: "Test content",
      link: "link"
    },
    {
      title: "Sessions per Day",
      subtitle: "Sessions per Week",
      content: "Test content",
      link: "link"
    },
    {
      title: "Average Queries",
      subtitle: "per Session",
      content: "Test content",
      link: "Link"
    },
    {
      title: "Average Session Duration",
      subtitle: "Test Subtitle",
      content: "Test content",
      link: "link"
    },
    {
      title: "Most Triggered Intent",
      subtitle: "Test Subtitle",
      content: "Test content",
      link: "link",
      sublink: ""
    },
    {
      title: "User's Exit-Point Intent",
      subtitle: "Test Subtitle",
      content: "Test content",
      link: "link",
      sublink: ""
    }
  ]
}
